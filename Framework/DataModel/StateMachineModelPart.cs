﻿using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using AssGameFramework.StateMachines;
using Godot;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Dynamic;
using System.Runtime.Serialization;
using AssGameFramework.Events;

namespace AssGameFramework.DataModel
{
    /// <summary>
    /// Template <see cref="ModelPart"/> for dealing with <see cref="StateMachineModel.StateMachineInstance"/>.
    /// </summary>
    /// <typeparam name="StateType">IConvertible, IFormattable, IComparable type that defines the states</typeparam>
    /// <typeparam name="EventType">IConvertible, IFormattable, IComparable type that defines the events</typeparam>
    public class StateMachineModelPart<StateType, EventType> : ModelPart, IModelMessageHandler<EventType>
        where StateType : struct, IConvertible, IFormattable, IComparable
        where EventType : ModelMessage
    {
        [Export]
        public StateType CurrentState { get => StateMachine.CurrentState; set{}}
        
        /// <summary>
        /// Get the <see cref="StateMachineModel.StateMachineInstance"/> held by this model.
        /// </summary>
        public StateMachineModel<StateType, EventType, Model>.Instance StateMachine { get; protected set; } = null;
        
        public StateMachineModelPart() : base()
        {
        }

        /// <summary>
        /// Updates the <see cref="StateMachineModel.StateMachineInstance"/>
        /// </summary>
        /// <param name="fixedStep">Delta time since last update</param>
        public override void _PhysicsProcess(float delta)
        {
            Debug.Assert(StateMachine != null, "State machine cannot be null.");
            StateMachine.UpdateStateMachine();
        }

        public virtual void HandleEvent(EventType mdlmsg)
        {
            StateMachine.AddEvent(mdlmsg);
        }
    }
}

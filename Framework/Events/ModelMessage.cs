using AssGameFramework.StateMachines;

namespace AssGameFramework.Events
{
    public abstract class ModelMessage : IStateMachineMessage
    {
        public virtual bool SupressLogs => false;
    }
}